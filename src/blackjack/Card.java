/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;
import java.awt.Image;

/**
 *
 * @author Bipul Bibhor Adhikari
 */
public class Card {
	private int suit;
	private int value;
	private Image image;

	public Card(int suit,int value,Image image){
		this.suit=suit;
		this.value=value;
		this.image=image;
	}
	
	public int getSuit() {
		return suit;
	}

	public int getValue() {
		return value;
	}

	public Image getImage() {
		return image;
	}

}
