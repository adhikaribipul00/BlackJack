/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import javax.imageio.ImageIO;

/**
 *
 * @author root
 */
public class Deck implements CardConstants{
	
	//Array to hold a card
	public Card cards[];
	//No. of cards used up from the deck
	private int count;
	
	public Deck(){
		//Init the deck
		cards = new Card[52];
		try{
			//Might not find the image file
			initDeck();
		}catch(IOException ex){
			ex.printStackTrace();
		}
		//Init count to 0
		count = 0;
		//Shuffle Cards
		shuffleCards();
	}
	
	public Card getCard() throws Exception{
		if (count<52)
		return cards[count++];
		else throw new Exception();
	}
	
	private void shuffleCards(){
		
		Random rnd=ThreadLocalRandom.current();
		
		for(int i=0;i<cards.length-1;i++){
			int index = rnd.nextInt(i+1);
			//Swapping Elements
			Card a = cards[index];
			cards[index] = cards[i];
			cards[i] = a;
		}
	}
	
	
	
	private Image getImage(int suit,int value) throws IOException{
		//Using value to string to match with the file names 
		//Simiar reason for using suitToString
		File cardImg=new File("./src/images/"+valueToString(value)+suitToString(suit)+".gif");
		//System.out.println(valueToString(value)+suitToString(suit));
		
		Image img=ImageIO.read(cardImg);
		return img;
	} 
	
	
	private void initDeck() throws IOException{
		//Create 52 cards using create Cards method
		for(int i=0;i<52;i++){
			if(i<13){
				int value=i;
				//Create Clubs
				cards[i]=new Card(CLUB,value,getImage(CLUB,value));
			}
			if(i>12 && i<26){
				//Create Spades
				int value=i-13;
				cards[i]=new Card(SPADE,value,getImage(SPADE,value));

			}
			if(i>25 && i<39){
				//Create Diamonds
				int value=i-26;
				cards[i]=new Card(DIAMONDS,value,getImage(DIAMONDS,value));

			}
			if(i>38 && i<52){
				//Create Hearts
				int value=i-39;
				cards[i]=new Card(HEARTS,value,getImage(HEARTS,value));

			}
		}
		
	}
	private String valueToString(int value){
		Integer val=++value;
		String str;
		if(val == 1) str="a";
		else if(val == 10) str="t";
		else if(val == 11) str="j";
		else if(val == 12) str="q";
		else if(val == 13) str="k";
		else str=String.valueOf(val);
		return str;
	}
	
	public String suitToString(int suit){
		/*
		*This method is required to convert suits to equivalent
		*file names inside the image folder.
		*/
		String str;
		switch(suit){
			case CLUB: str="c";
				break;
				
			case DIAMONDS: str="d";
				break;
				
			case SPADE: str="s";
				break;
				
			case HEARTS: str="h";
				break;
			
			default: str=null;
				 break;
		}
		return str;
	}
	
	/*
	public static void main(String args[]){
		Deck deck = new Deck();
		for (int  i = 0; i < 52; i++)
			System.out.println("CARD="+deck.cards[i].getSuit() + deck.cards[i].getValue());
	}
*/

}
