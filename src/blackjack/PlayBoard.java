/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author bipgen
 */
public class PlayBoard extends JPanel {
    
    private Shape scoreBox;
    private JButton hit;
    private JButton doubleDown;
    private JButton stand;
    
    private Deck deck;
    private Player dealer;
    private Player player;
    
    private boolean showCardsInScreen = false;
    private boolean showDealerCard = false;
    
    private boolean playerWin = false;
    
    /**
     * 
     * @param player
     * Player is init. from main method and passed
     * to our payboard class
     * Init. dealer player and calls startPlay()
     */
    public PlayBoard(Player player){
        super();
        dealer = new Player("Dealer");
        //Setting up our player
        this.player = player;
        //Give cards and init other components
        initComponents();
    }
    
    private void initComponents() {
        //Set size of the panel
        setSize(600,300);
        //Setting the layout to borderlayout
        setLayout(new BorderLayout());
        //Set up a new game scene
        newGame();
        //Add the stat bar for the player
        setVisible(true);
       // addStatBar();
    }
          
    /**
     * Method clears players and dealers both of
     * the hands and set's the bet panel
     * 
     */
    private void newGame(){
        //Clear the hand for both the player and the 
       
        if (playerWin) scoreWin(player);
        player.clearHand();
        dealer.clearHand();
        showDealerCard = false;
        
        if (btnPanel != null) removeBtnPanel();
        try{
            startPlay();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        revalidate();
        repaint();
        revalidate();
        showBet();
         addStatBar();
    }
    
    private void startPlay() throws Exception{
        /*
        Init the dealer var too.    
        Give 2 cards to each player and the dealer
        */
        
        deck = new Deck();
        giveCards(2,player);
        giveCards(2,dealer);
        
        showCardsInScreen = true;
    }
    
    
    
    private void scoreWin(Player user){
        int betAmount =(Integer) currency.getSelectedItem();
        double winAmt = betAmount * 1.5 + player.getCash();
        user.setCash((int)winAmt);
        
    }
    
    private void scoreLoss(Player user){
        int betAmount = (Integer) currency.getSelectedItem();
        double loseAmt = player.getCash() - betAmount;
        user.setCash((int)loseAmt);
    }
    
    private void showCardsInScreen(Graphics2D g){
        double playerCardX = ( getWidth()) * (double)(1.5/4.0);
        double playerCardY = ( getHeight()) * (double)(3.0/5.0);
        
        double dealerCardX = ( getWidth()) * (double)(1.5/4.0);
        double dealerCardY = ( getHeight()) * (double)(1.0/5.0);
          
        for ( int i = 0; i < player.getHand().size(); i++){
            g.drawImage(player.getHand().get(i).getImage(),(int)playerCardX,(int)playerCardY, this);
            playerCardX+=80;
        }
        
        for ( int i = 0; i < dealer.getHand().size(); i++){
           if ( i < 1 ){
            g.drawImage(dealer.getHand().get(i).getImage(),(int)dealerCardX,(int)dealerCardY, this);
            dealerCardX+=80;
            }
            else {
               if( showDealerCard){
                   g.drawImage(dealer.getHand().get(i).getImage(),(int)dealerCardX,(int)dealerCardY, this);
                   dealerCardX+=80;
               }
               else
                 g.drawImage(getCardBack(),(int)dealerCardX,(int)dealerCardY, this);
            }
        }
                    
    }
    
    private Image getCardBack(){
        File cardImg=new File("./src/images/"+ "b" +  ".gif");
                Image img = null;
                try {
                    img = ImageIO.read(cardImg);
                    } catch (IOException ex){ 
                        ex.printStackTrace();
                    }
                return img;
               
        
    }
    
   
    
    private JPanel betPanel;
    private JComboBox currency;
    
    private void showBet(){
        
        JLabel betAmount;
        JButton bet;
        //Creating a bet bar for the initial part of the game
        Object[] betAmounts = {new Integer(50),new Integer(100),new Integer(200),new Integer(500),new Integer(1000)};
        DefaultComboBoxModel model = new DefaultComboBoxModel(betAmounts);
        currency = new JComboBox(model);
        
        betAmount = new JLabel("Bet");
        bet = new JButton("Bet");
        
        /*
        Putting these stuffs
        in the east panel
        */
        if (statBar!= null) remove(statBar);
        betPanel = new JPanel(new FlowLayout());
        betPanel.add(betAmount);
        betPanel.add(currency);
        betPanel.add(bet);
        add(betPanel,BorderLayout.SOUTH);
        
        bet.addActionListener((ActionEvent e) -> {
            /*
            Remove the bet bar
            Add buttons for stand,double and hit
            Repaint and revalidate
            */
            
            do{
                if( !setBet() ) JOptionPane.showMessageDialog(this,"You are betting more than you have!");
                if ( !setBet() ) newGame();
            }while( !setBet());
            removeBet();
            addButtons();
            invalidate();
            repaint();
            
            revalidate();
        });
        repaint();
        revalidate();
        
        
    }
    
    private boolean setBet(){
        if( ( (int)currency.getSelectedItem() ) < player.getCash() )
        {
            player.setBetAmount((int)currency.getSelectedItem());
            return true;
        }
        else return false;
        
    }
    
    private void removeBet(){
        remove(betPanel);
        betPanel.invalidate();
        invalidate();
        repaint();
        validate();   
        this.revalidate();
        
    }
       
    private JPanel statBar;
    
    private void addStatBar(){
        
        String[] ColumnHeaders = { "",""};
        Object[][] data = {{"Name", player.getName()},
            {"Cash", player.getCash()},
            {"Hand Value",player.handValue()},
            {"Win prob." , getPlayerWinProb().toString()+"%" }
        };
        
        JTable playerDetails = new JTable(data,ColumnHeaders){
            @Override
            public Dimension getPreferredScrollableViewportSize(){
                return new Dimension(100,100);
                
            }
            
        };
         playerDetails.setShowGrid(false);

        
        JScrollPane pane = new JScrollPane(playerDetails);
        
        statBar = new JPanel(new BorderLayout());
        JLabel label = new JLabel("Player Status");
        statBar.add(label,BorderLayout.NORTH);
        statBar.add(pane,BorderLayout.CENTER);
        add(statBar,BorderLayout.EAST);
        repaint();
       revalidate();
        }
    
    private Integer getPlayerWinProb(){
        return 12;
    }
    
    private void removeBtnPanel(){
        remove(btnPanel);
        btnPanel.invalidate();
        repaint();
        revalidate();
    }
    private JPanel btnPanel;
    
    private void addButtons(){
        doubleDown = new JButton("Double Down");
        stand = new JButton("Stand");
        hit = new JButton("Hit");
        
        btnPanel = new JPanel();
        btnPanel.add(hit);
        btnPanel.add(doubleDown);
        btnPanel.add(stand);
        
        hit.addActionListener(new HitListener());
        doubleDown.addActionListener(new DoubleDownListener());
        stand.addActionListener(new StandListener());
        
        
        add(btnPanel,BorderLayout.SOUTH);
        
        repaint();
    }
    
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        int w = getWidth();
        int h = getHeight()/2;
        Color color1 = Color.black;
        Color color2 = Color.GREEN;
        GradientPaint gp = new GradientPaint(0, 0, color1, 0, h, color2);
        g2d.setPaint(gp);
        g2d.fillRect(0, 0, w, h);
        gp = new GradientPaint(w,h,color2,w,h*2,color1);
        g2d.setPaint(gp);
        g2d.fillRect(0,h,getWidth(),getHeight());
       
        createRings(g2d);
        showCards(g2d);
        
       // if (showCardsInScreen)
       showCardsInScreen(g2d);
        //g2d.fillRect(0,0,w,h);
        
    }
    
    private void createRings(Graphics2D g){
        //Creating ovals
        int wFix = getWidth()/4;
        int hFix = getHeight()/3;
        int x = getWidth() / 10;
        int y = getHeight() / 10;
        int w =(int) (getWidth() - wFix);
        int h = (int) (getHeight() - hFix);
        int arcW = getWidth()/2;
        int arcH = getHeight()/2;
        g.setColor(Color.YELLOW);
        g.setStroke(new BasicStroke(10));
        g.drawRoundRect(x,y,w,h,arcW,arcH);
        
        createInnerRing(g);
    }
    
    private void createInnerRing(Graphics2D g){
         int wFix = getWidth()/8;
        int hFix = getHeight()/6;
        int x = getWidth() / 31;
        int y = getHeight() / 40;
        int w =(int) (getWidth() - wFix);
        int h = (int) (getHeight() - hFix);
        int arcW = getWidth()/3;
        int arcH = getHeight()/3;
        g.setColor(Color.YELLOW);
        g.setStroke(new BasicStroke(10));
        g.drawRoundRect(x,y,w,h,arcW,arcH);
    }
    
    private void showCards(Graphics2D g){
        
        for ( int i = 0 ; i < 10; i++){
            
            
            int xFix = 200+ (i*2);
            int yFix = 65;
            int x = getWidth()/2 + xFix;
            int y = getHeight()/2 - yFix;
            g.drawImage(getCardBack(),x,y,this);
            
            
        }
        
   
    }
    
    private void giveCards(int totalCards,Player player) throws Exception{
       for ( int i = 0 ; i < totalCards; i++)   {
            player.addCard(deck.getCard());
//hand.add(deck.getCard());
        }
        
    }
    
    private void checkBust(){
        if (player.isBust() ) {
            scoreLoss(player);
            gamePoint("Game over. Would you like to play again?");
        }
    }
    
    private void gamePoint(String message){
       // System.out.println("Game over");
        /*
        Creating a dialog box asking for retry or quit
        */
        Object[] options = { "Yes", "No"};
        int choice = showDialogBox(message,options);
        if (choice == 0)
            newGame();
        if (choice == 1){
            exitGame();
        }
    }
    
   
    
    private void exitGame(){
        System.exit(0);
        
    }
    
    private int showDialogBox(String message,Object[] options){
        
    
                        
    int n = JOptionPane.showOptionDialog(this,
    message,
    "",
    JOptionPane.YES_NO_OPTION,
    JOptionPane.QUESTION_MESSAGE,
    null,
    options,
    options[0]);
    return n;
    }
    
    private Integer winCalculation(){
        return 0;
    }

    
    private void checkForStand(){
        showDealerCard = true;
        repaint();
        
        
           while(!dealer.isBust() && !(dealer.handValue() > player.handValue()) ){
            try {
                giveCards(1,dealer);
            } catch (Exception ex) {
                Logger.getLogger(PlayBoard.class.getName()).log(Level.SEVERE, null, ex);
            }
           }
           if( dealer.isBust()){
               
               playerWin = true;
               gamePoint("Congratulations. You've won" + winCalculation().toString() + " Would you like to play again?");
               
        }
           else{
               playerWin = false;
               scoreLoss(player);
                gamePoint("Oops! You've lost." + winCalculation().toString() + " Would you like to play again?");
           }
    }
    
    
    
    
    class HitListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                giveCards(1, player);
            } catch (Exception ex) {
                Logger.getLogger(PlayBoard.class.getName()).log(Level.SEVERE, null, ex);
            }
            repaint();
            checkBust();
        }
    
    }
    
    class DoubleDownListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                giveCards(1, player);
                
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }
    
    class StandListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            checkForStand();
        }
        
    }
    
    
}
