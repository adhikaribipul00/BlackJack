/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

import java.util.ArrayList;

/**
 *
 * @author root
 */
public class Player {
	private String name;
	private ArrayList<Card> hand;
	private boolean bust;
        private int cash;
        private int betAmount;
	
	public Player(String name)
	{
		this.name = name;
                hand = new ArrayList<Card>();
	}
        
        public void setCash(int cash){
            this.cash = cash;
        }
	
	public void putHand(Card[] hand){
		for ( int i = 0; i < hand.length; i++){
                    this.hand.add(hand[i]);
                }
	}
        
        public void clearHand(){
            hand = new ArrayList<Card>();
        }
        
        public void addCard(Card card){
            hand.add(card);
        }
	
	public ArrayList<Card> getHand(){
		return hand;
	}
	
	public int handValue(){
		/**
		 * this value is respective to
		 * the game. For blackjack it returns
		 * the value
		 */
		int value = 0;
//                Card[] handArr = (Card [])hand.toArray();
		for (int i = 0; i < hand.size(); i++){
			if (hand.get(i).getValue() > 9){
				value += 10;
			}
			else if ( hand.get(i).getValue() == 0 ){
				if ( value + 10 <= 21 ) value = value + 10;
				else value++;
			}
			else{
				value += ( hand.get(i).getValue() + 1 );
			}
		}
		return value;
	}

	public boolean isBust(){
		if ( handValue() > 21) return true;
		else return false;
	}

    public String getName() {
        return name;
    }

    public int getCash() {
        return cash;
    }
    
    public void setBetAmount(int amt){
        this.betAmount = amt;
        
    }
    
    public int getBetAmount(){
        return betAmount;
    }
        
}
