/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
/**
 *
 * @author bipgen
 */
public class EntryScreen extends JPanel{
    
    public EntryScreen(){
        super();
        setSize(500,300);
        //Setting the background color
        this.setBackground(Color.RED);
        
        
    }
    
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        File cardImg=new File("./src/images/"+ "bg" +  ".jpg");
        Image img;
        try {
            img = ImageIO.read(cardImg);
            
            g.drawImage(img,0,0,this);
        } catch (IOException ex) {
            Logger.getLogger(EntryScreen.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String title = "BLACKJACK";
        g.setFont(new Font("Helvetica",Font.BOLD,45));
        g.setColor(Color.BLACK);
        int X = 10;
        int Y = getHeight()/5 + 100;
        g.drawString(title,X, Y);
        
        
    }
}
