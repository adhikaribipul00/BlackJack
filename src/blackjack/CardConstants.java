/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

/**
 *
 * @author root
 */
public interface CardConstants {


//Values possible in a deck.	
	public static final int ACE=1;
	public static final int TWO=2;
	public static final int THREE=3;
	public static final int FOUR=4;
	public static final int FIVE=5;
	public static final int SIX=6;
	public static final int SEVEN=7;
	public static final int EIGHT=8;
	public static final int NINE=9;
	public static final int TEN=10;
	public static final int JACK=11;
	public static final int QUEEN=12;
	public static final int KING=13;
//Values for the different SUITS
	public static final int CLUB=0;
	public static final int HEARTS=1;
	public static final int SPADE=2;
	public static final int DIAMONDS=3;

}
